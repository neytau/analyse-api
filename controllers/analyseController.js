import moment from 'moment'
import { client } from '../conf/redisConf'
import { queue } from '../conf/rabbitmqConf'

const addToQueue = (q, mess) => {
    queue.then(con => con.createChannel())
    .then(ch => {
        return ch.assertQueue(q).then(ok => {
            return ch.sendToQueue(q, new Buffer(mess))
        })
    }).catch(console.warn)
}

const analyze = (msg, ack) => {
    client.MULTI()
    .LLEN(`${msg}:clicked`)
    .LLEN(`${msg}:mouved`)
    .LLEN(`${msg}:emotion`)
    .HGET(msg, 'alerted')
    .HGET(msg, 'viewedAt')
    .HSET(msg, 'watchedAt', Date.now())
    .EXEC((err, reply) => {
        let weight = 0
        if (reply[0] > 40) {
            weight += reply[0] * 0.15
        }
        if (reply[1] > 40) {
            weight += reply[1] * 0.05
        }
        if (reply[2] > 40) {
            weight += reply[2] * 0.3
        }
        if (parseInt(reply[4]) && moment().diff(moment(parseInt(reply[4])), 'seconds') > 200) {
            weight += moment().diff(moment(parseInt(reply[4])), 'seconds') * 0.4
        }

        if (!parseInt(reply[3]) && weight >= 5) {
            client.HSET(msg, 'alerted', Date.now(), (err, rep) => {
                if (err) throw err
                addToQueue('alert', msg)
            })
        } else if (parseInt(reply[3]) && moment().diff(moment(parseInt(reply[3])), 'minutes') >= 3 && weight >= 5) {
            client.MULTI()
            .HSET(msg, 'alerted', 0)
            .DEL(`${msg}:clicked`)
            .DEL(`${msg}:mouved`)
            .DEL(`${msg}:tookPicture`)
            .DEL(`${msg}:typed`)
            .EXEC((err, rep) => {
                if (err) throw err
                addToQueue('askedHelp', msg)
            })
        }
    })
    return ack
}

exports.getAnalyze = (msg, ack) => {
    client.HGET(msg, 'watchedAt', (err, rep) => {
        if (err) throw err
        if (!rep) {
            analyze(msg, ack)
        }
        const date = moment(parseInt(rep))
        if (moment().diff(date, 'seconds') > 10) {
            analyze(msg, ack)
        }
        return ack()
    })
}