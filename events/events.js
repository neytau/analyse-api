import analyseController from '../controllers/analyseController'
import { queue } from '../conf/rabbitmqConf'

module.exports = server => {

    queue.then(con => con.createChannel())
    .then(ch => {
        const q = 'event'
        return ch.assertQueue(q).then(ok => {
            return ch.consume(q, msg => {
                if (msg !== null) {
                    const ack = () => ch.ack(msg)
                    analyseController.getAnalyze(msg.content.toString(), ack)
                }
            })
        })
    }).catch(console.warn)
}