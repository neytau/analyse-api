# Analyse-API

Interprète les actions des utilisateurs.

# Developement setup

Renseigner les variables d'environnement.

> Exemple .env: 
```
HOST=
PORT=
REDIS_HOST=
REDIS_PORT=
REDIS_PASS=
RABBITMQ_HOST=
```

# Getting started

Vous pouvez lancer cette commande dans votre terminal:

```
npm start
```